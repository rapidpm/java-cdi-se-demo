package org.rapidpm.demo.cdise.controller.registry.services;

/**
 * User: Sven Ruppert
 * Date: 05.06.13
 * Time: 15:36
 */
public interface Service {

    public String execute();

}
