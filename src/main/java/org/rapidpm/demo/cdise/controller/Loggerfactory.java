package org.rapidpm.demo.cdise.controller;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 30.04.13
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */
public class Loggerfactory {

    @Produces
    Logger createLogger(InjectionPoint injectionPoint) {
        final Class<?> declaringClass = injectionPoint.getMember().getDeclaringClass();
        return Logger.getLogger(declaringClass);
    }
}
