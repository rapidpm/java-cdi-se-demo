package org.rapidpm.demo.cdise.controller.registry;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.weld.environment.se.WeldContainer;
import org.rapidpm.demo.cdise.controller.registry.services.Service;

/**
 * User: Sven Ruppert
 * Date: 05.06.13
 * Time: 15:38
 */
@ApplicationScoped
public class ServiceRegistry {


    private WeldContainer weldContainer;

    @Inject @Default
    Logger logger;


    private List<Class<? extends Service>> services = new ArrayList<>();

    public ServiceRegistry() {

    }

    @PostConstruct
    public void init() {
        //durch PkgClassLoader realisieren
        try {
            final Class[] classes = getClasses("org.rapidpm.demo.cdise.controller.registry.services");
            for (final Class aClass : classes) {
                final Annotation annotation = aClass.getAnnotation(RegisteredService.class);
                if (annotation != null) {
                    services.add(aClass);
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("keine Annotation");
                    }
                }
            }
        } catch (ClassNotFoundException | IOException e) {
            logger.error(e);
        }
    }


    public List<Service> getManagedServices(){
        final List<Service> resuList = new ArrayList<>();
        for (final Class<? extends Service> service : services) {
            final Instance<Object> instance = weldContainer.instance();
            final Instance<? extends Service> select = instance.select(service);
            final Service e = select.get();
            resuList.add(e);
        }
        return resuList;
    }


    public void setWeldContainer(WeldContainer weldContainer) {
        this.weldContainer = weldContainer;
    }

    //simuliert einen pkg classloader
    private Class[] getClasses(String packageName) throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        final String path = packageName.replace('.', '/');
        final Enumeration<URL> resources = classLoader.getResources(path);
        final List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            final URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        final List<Class> classes = new ArrayList<>();
        for (final File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    private List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        final List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        final File[] files = directory.listFiles();
        for (final File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}
