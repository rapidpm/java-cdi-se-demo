package org.rapidpm.demo.cdise.controller.registry.services;

import javax.inject.Inject;

import org.rapidpm.demo.cdise.controller.registry.RegisteredService;
import org.rapidpm.demo.cdise.controller.service.AddService;

/**
 * User: Sven Ruppert
 * Date: 05.06.13
 * Time: 15:36
 */
@RegisteredService
public class ServiceB implements Service  {

    @Inject  //als Beispiel das diese instanz gemanaged ist
    private AddService addService;

    @Override
    public String execute() {
        return this.getClass().getName() + "  --  "+ addService.add(1, 3);
    }
}
