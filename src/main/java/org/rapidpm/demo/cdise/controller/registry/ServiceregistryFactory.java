package org.rapidpm.demo.cdise.controller.registry;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * User: Sven Ruppert
 * Date: 05.06.13
 * Time: 16:15
 */
public class ServiceregistryFactory {

    private static final WeldContainer container = new Weld().initialize();
    private static ServiceRegistry instance = container.instance().select(ServiceRegistry.class).get();

    public static ServiceRegistry getInstance() {
        instance.setWeldContainer(container);
        return instance;
    }



}
