package org.rapidpm.demo.cdise.controller.registry.services;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.New;
import javax.enterprise.inject.Produces;

/**
 * User: Sven Ruppert
 * Date: 05.06.13
 * Time: 16:03
 */
@ApplicationScoped
public class ServiceProducer {


    @Produces
    public ServiceA createInstanceA(@New ServiceA service){
        return service;
    }
    @Produces
    public ServiceB createInstanceB(@New ServiceB service){
        return service;
    }
    @Produces
    public ServiceC createInstanceC(@New ServiceC service){
        return service;
    }



}
