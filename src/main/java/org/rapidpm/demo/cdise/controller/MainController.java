package org.rapidpm.demo.cdise.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.rapidpm.demo.cdise.controller.service.AddService;
import org.rapidpm.demo.cdise.controller.service.MultService;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 30.04.13
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */

@ApplicationScoped
public class MainController {

    @Inject @Default
    private Logger logger;
    @Inject private AddService addService;
    @Inject private MultService multService;

    public AddService getAddService() {
        return addService;
    }

    public MultService getMultService() {
        return multService;
    }
}
