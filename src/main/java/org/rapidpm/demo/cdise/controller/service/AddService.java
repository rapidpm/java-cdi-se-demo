package org.rapidpm.demo.cdise.controller.service;

import javax.inject.Inject;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 30.04.13
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */
public class AddService {

    @Inject Logger logger;

    public int add(int a, int b){
        return a+b;
    }
}
