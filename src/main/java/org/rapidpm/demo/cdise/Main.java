package org.rapidpm.demo.cdise;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.rapidpm.demo.cdise.controller.MainController;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 30.04.13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    private final WeldContainer weldContainer = new Weld().initialize();

    //@Inject hier nicht wegen BootStrap Weld
    private final Logger logger = weldContainer.instance().select(Logger.class).get();
    //@Inject private Logger logger;

    //@Inject hier nicht wegen BootStrap Weld
    private MainController controller = weldContainer.instance().select(MainController.class).get();


    public static void main(String[] args) {
        //.select( Application.class ).get();
        final Main main = new Main();
        final int i = main.controller.getAddService().add(1, 2);
        System.out.println("i = " + i);
        main.logger.debug("i = " + i);

        final int mult = main.controller.getMultService().mult(1, 2);
        System.out.println("mult = " + mult);

    }
}
