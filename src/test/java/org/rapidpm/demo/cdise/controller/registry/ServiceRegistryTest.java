package org.rapidpm.demo.cdise.controller.registry;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.rapidpm.demo.cdise.controller.registry.services.Service;

/**
 * ServiceRegistry Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Jun 5, 2013</pre>
 */
public class ServiceRegistryTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: getInstance()
     */
    @Test
    public void testGetInstance() throws Exception {
        final ServiceRegistry instance = ServiceregistryFactory.getInstance();
        Assert.assertNotNull(instance);
    }

    /**
     * Method: getManagedServices()
     */
    @Test
    public void testGetManagedServices() throws Exception {
        final ServiceRegistry instance = ServiceregistryFactory.getInstance();
        Assert.assertNotNull(instance);

        final List<Service> managedServices = instance.getManagedServices();
        Assert.assertNotNull(managedServices);
        Assert.assertFalse(managedServices.isEmpty());
        for (final Service managedService : managedServices) {
            Assert.assertNotNull(managedService);
            final String execute = managedService.execute();
            System.out.println("execute = " + execute);
        }
    }


    /**
     * Method: getClasses(String packageName)
     */
    @Test
    public void testGetClasses() throws Exception {
//TODO: Test goes here... 
/* 
try { 
   Method method = ServiceRegistry.getClass().getMethod("getClasses", String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/
    }

    /**
     * Method: findClasses(File directory, String packageName)
     */
    @Test
    public void testFindClasses() throws Exception {
//TODO: Test goes here... 
/* 
try { 
   Method method = ServiceRegistry.getClass().getMethod("findClasses", File.class, String.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/
    }

} 
